package by.nastya.utils;

public abstract class IdentityStorage {
    public static final Long OPERATION_TYPE_PARENT = 1L;

    public static final Long INCOMING_TYPE = 2L;

    public static final Long CONSUMPTION_TYPE = 3L;

    public static final Long SHOP_SUBTYPE = 6L;
}
