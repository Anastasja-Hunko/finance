package by.nastya.utils;

import org.springframework.web.servlet.ModelAndView;

public class View extends ModelAndView {
    public View(String viewName) {
        super("template/layout", "content", "../" + viewName + ".jsp");
    }
}