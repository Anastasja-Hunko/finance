package by.nastya.utils;

import java.util.Calendar;
import java.util.Date;

public enum Month {
    January("Январь"),
    February("Февраль"),
    March("Март"),
    April("Апрель"),
    May("Май"),
    June("Июнь"),
    July("Июль"),
    August("Август"),
    September("Сентябрь"),
    October("Октябрь"),
    November("Ноябрь"),
    December("Декабрь");

    private final String name;

    public static Month getByDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return Month.values()[calendar.get(Calendar.MONTH)+1];
    }

    public static Month forCode(int code) {
        for (Month e : Month.values()) {
            if (e.ordinal() == code)
                return e;
        }
        return null;
    }

    public static  Month forName(String name) {
        for (Month e : Month.values()) {
            if (e.toString().equals(name))
                return e;
        }
        return null;
    }

    public String getName() {
        return this.name;
    }

    public int getCode() {
        return ordinal();
    }

    Month(String s) {
        name = s;
    }

    public String toString(){
        return name ;
    }
}