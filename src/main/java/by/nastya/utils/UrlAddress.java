package by.nastya.utils;

public class UrlAddress {
    public static final String URL_ROOT = "/";
    public static final String URL_LOGIN = "/login";
    public static final String URL_REGISTRATION = "/registration";
}
