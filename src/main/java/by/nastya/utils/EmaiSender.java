package by.nastya.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;

/**
 * Created by nastya on 19.05.2017.
 */
@Component
public class EmaiSender {
    @Autowired
    private JavaMailSender mailSender;

    public void sendMail(String from, String to, String subject, String msg) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            message.setSubject(subject);
            MimeMessageHelper helper;
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setText(msg, true);
            mailSender.send(message);
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }
    }
}
