package by.nastya.controllers;

import by.nastya.services.SupplyService;
import by.nastya.utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class SupplyListController {
    @Autowired
    private SupplyService supplyService;

    @RequestMapping(value = "/supplyList", method = RequestMethod.GET)
    public ModelAndView getSupplyList(){
        View view = new View("supplyList");
        view.addObject("lists", supplyService.findAll());
        return view;
    }

    @RequestMapping(value = "/addlist", method = RequestMethod.GET)
    public ModelAndView addList(){
        View view = new View("addlist");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        view.addObject("current", format.format(date));
        return view;
    }

    @RequestMapping(value = "/addlist", method = RequestMethod.POST)
    public String saveList(@RequestParam("listname") String name){
        supplyService.saveList(name);
        return "redirect:/supplyList";
    }

    @RequestMapping(value = "/listContent/{id}", method = RequestMethod.GET)
    public ModelAndView getContent(@PathVariable(value="id") Long id){
        View view = new View("listcontent");
        view.addObject("list", supplyService.getSuppliesList(id));
        view.addObject("supplyid", id);
        return view;
    }

    @RequestMapping(value = "/addsupply", method = RequestMethod.GET)
    public ModelAndView addProduct(HttpServletRequest request){
        View view = new View("addsupply");
        view.addObject("supplyid", request.getParameter("per"));
        return view;
    }

    @RequestMapping(value = "/addsupply", method = RequestMethod.POST)
    public String saveProduct(@RequestParam("id") Long id, @RequestParam("name") String name, @RequestParam("cost") BigDecimal cost){
        supplyService.saveSupply(id, cost, name);
        return "redirect:/listContent/"+id;
    }

    @RequestMapping(value = "/qqq", method = RequestMethod.POST)
    public String toConsumption(Long param) {
        supplyService.transferForConsumption(param);
        return "redirect:/supplyList";
    }

    @RequestMapping(value = "/deletelist", method = RequestMethod.POST)
    @ResponseBody
    public String deleteList(Long id){
        supplyService.deleteList(id);
        return "redirect:/supplyList";
    }

    @RequestMapping(value = "/addPrice", method = RequestMethod.POST)
    @ResponseBody
    public void addPrice(@RequestParam("id") Long id, @RequestParam("value") BigDecimal sum){
        supplyService.changePrice(id, sum);
    }
}
