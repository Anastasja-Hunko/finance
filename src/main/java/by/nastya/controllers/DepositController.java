package by.nastya.controllers;

import by.nastya.models.Deposit;
import by.nastya.models.DepositInfo;
import by.nastya.repositories.DepositInfoRepository;
import by.nastya.repositories.DepositRepository;
import by.nastya.services.DepositService;
import by.nastya.services.WorkWithDateService;
import by.nastya.utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Controller
public class DepositController {
    @Autowired
    private DepositInfoRepository depositInfoRepository;
    @Autowired
    private DepositRepository depositRepository;
    @Autowired
    private DepositService depositService;
    @Autowired
    private WorkWithDateService workWithDateService;

    @RequestMapping(value = "/deposit/add", method = RequestMethod.GET)
    public ModelAndView addList(){
        View view = new View("addDeposit");
        return view;
    }

    @RequestMapping(value = "/deposit/add", method = RequestMethod.POST)
    public String saveList(@RequestParam("startSum") String startSum, @RequestParam("percent") Float percent){
        depositService.saveDeposit(startSum, percent);
        return "redirect:/management";
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.GET)
    public ModelAndView getDepositStartForm(){
        View view = new View("deposit");
        List<Deposit> depositList = depositRepository.findAll();
        if(depositList.isEmpty()) {
            Deposit deposit = depositList.get(0);
            view.addObject("beginSum", deposit.getBeginSum());
            view.addObject("date", workWithDateService.formatParamDate(deposit.getDatebegin()));
            Date dateEnd = workWithDateService.addMonthesToDate(12, deposit.getDatebegin());
            view.addObject("dateEnd", workWithDateService.formatParamDate(dateEnd));
            view.addObject("percent", deposit.getPercent());

            if(depositInfoRepository.count() == 0) {
                depositService.work(deposit);
            }
            List<DepositInfo> infoList = depositInfoRepository.findAll();
            view.addObject("info", infoList);

            BigDecimal sum = depositInfoRepository.getIntermediateSumByDateDeposit(dateEnd, deposit.getId());

            view.addObject("total", sum);
        } else {
            view.addObject("message", "У вас не открыт ни один депозит");
        }
        return view;
    }
}
