package by.nastya.controllers;

import by.nastya.models.User;
import by.nastya.repositories.*;
import by.nastya.utils.View;
import by.nastya.services.UserService;
import by.nastya.utils.UrlAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = UrlAddress.URL_LOGIN, method = RequestMethod.GET)
    public ModelAndView login(String error) {
        View view = new View("login");
        if (error != null)
            view.addObject("error", true);
        return view;
    }

    @RequestMapping(value = UrlAddress.URL_REGISTRATION, method = RequestMethod.GET)
    public ModelAndView registrationGet() {
        return new View("registration");
    }

    @RequestMapping(value = UrlAddress.URL_REGISTRATION, method = RequestMethod.POST)
    public String registrationPost(@RequestParam("username") String login,
                                   @RequestParam("password") String password, @RequestParam("email") String email, RedirectAttributes ra) {
        Boolean status = userService.registration(login, password, email);
        if(status) {
            ra.addFlashAttribute("message", "Регистрация прошла успешно.");
        } else {
            ra.addFlashAttribute("message", "Пользователь с данным логином уже существует");
        }
        return "redirect:" + UrlAddress.URL_REGISTRATION;
    }

    @RequestMapping(value = "/restore", method = RequestMethod.GET)
    public ModelAndView restore() {
        return new View("restore/requestForm");
    }

    @RequestMapping(value = "/restore", method = RequestMethod.POST)
    public ModelAndView restorePost(@RequestParam("email") String email,
                                    HttpServletRequest request,
                                    RedirectAttributes redirectAttributes) {
        User user = userRepository.findByEmail(email);
        if(user != null) {
            if(userService.sendRestoreRequest(user, request)){
                redirectAttributes.addFlashAttribute("info", "Ссылка для восстановления доступа отправлена на <b>" + user.getEmail() + "</b>");
            } else {
                redirectAttributes.addFlashAttribute("error", "Произошла ошибка");
            }
        } else {
            redirectAttributes.addFlashAttribute("error", "Пользователь с указанным email не найден");
        }

        return new ModelAndView("redirect:/restore");
    }

    @RequestMapping(value = "/restore/confirm/{token}", method = RequestMethod.GET)
    public ModelAndView confirmRestore(@PathVariable("token") String token) {
        View view = new View("restore/setPassword");
        view.addObject("title", "Восстановление доступа");
        User user = userRepository.findByToken(token);
        if(user != null) {
            view.addObject("token", token);
        } else {
            view.addObject("error", "Ссылка для восстановления не верна, либо просрочена");
            view.addObject("hideForm", true);
        }
        return view;
    }

    @RequestMapping(value = "/restore/confirm/{token}", method = RequestMethod.POST)
    public String confirmRestorePost(@PathVariable("token") String token,
                                     @RequestParam("password") String password,
                                     RedirectAttributes redirectAttributes) {
        User user = userRepository.findByToken(token);
        if(user != null) {
            if(userService.changePassword(user, password)) {
                redirectAttributes.addFlashAttribute("success", "Поздравляем. Теперь вы можете войти в систему, используя Ваш логин и измененный пароль");
                return "redirect:/login";
            } else {
                redirectAttributes.addFlashAttribute("error", "Старый и новый пароли совпадают, придумайте новый пароль");
                return "redirect:/restore/confirm/" + token;
            }
        }
        return "redirect:/restore";
    }
}


