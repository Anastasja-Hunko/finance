package by.nastya.controllers;

import by.nastya.models.*;
import by.nastya.repositories.DepositRepository;
import by.nastya.repositories.DictionaryRepository;
import by.nastya.repositories.StorageRepository;
import by.nastya.utils.IdentityStorage;
import by.nastya.utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ManagementController {
    @Autowired
    StorageRepository storageRepository;
    @Autowired
    DictionaryRepository dictionaryRepository;
    @Autowired
    DepositRepository depositRepository;

    @RequestMapping(value = "/management", method = RequestMethod.GET)
    public ModelAndView getManagment() {
        View view = new View("management");
        List<Dictionary> operationList = dictionaryRepository.findByParentId(IdentityStorage.INCOMING_TYPE);
        operationList.addAll(dictionaryRepository.findByParentId(IdentityStorage.CONSUMPTION_TYPE));
        view.addObject("list", operationList);
        List<Deposit> depositList = depositRepository.findAll();
        view.addObject("depositlist", depositList);
        List<Storage> storageList = storageRepository.findAll();
        view.addObject("storageList", storageList);
        return view;
    }

    @RequestMapping(value = "/addsource", method = RequestMethod.GET)
    public ModelAndView addList(){
        View view = new View("addsource");
        List<Dictionary> list = dictionaryRepository.findByParentId(IdentityStorage.OPERATION_TYPE_PARENT);
        view.addObject("qwelist", list);
        return view;
    }

    @RequestMapping(value = "/addsource", method = RequestMethod.POST)
    public String saveList(@RequestParam("source") String name, @RequestParam("operation") Long operation){
        Dictionary source = new Dictionary();
        source.setName(name);
        source.setParent(dictionaryRepository.getOne(operation));
        dictionaryRepository.save(source);
        return "redirect:/management";
    }
}
