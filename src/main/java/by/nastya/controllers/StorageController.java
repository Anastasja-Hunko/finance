package by.nastya.controllers;

import by.nastya.services.StorageService;
import by.nastya.utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StorageController {
    @Autowired
    private StorageService storageService;

    @RequestMapping(value = "/storages", method = RequestMethod.GET)
    public ModelAndView getStorages() {
        View view = new View("storage");
        view.addObject("storages", storageService.getStorages());
        return view;
    }

    @RequestMapping(value = "/storage/add", method = RequestMethod.GET)
    public ModelAndView addList(){
        View view = new View("addStorage");
        return view;
    }

    @RequestMapping(value = "/storage/add", method = RequestMethod.POST)
    public String saveList(@RequestParam("storageName") String name, @RequestParam("storageLimit") String limit, @RequestParam("percent") Float percent){
        storageService.saveStorage(name, limit, percent);
        return "redirect:/storages";
    }

    @ResponseBody
    @RequestMapping(value = "/changePercent", method = RequestMethod.POST)
    public void changeStoragePercent(@RequestParam("rowId")Long id, @RequestParam("value") String value){
        Float percent;
        try {
            percent = Float.valueOf(value);
        } catch (Exception ex) {
            percent = 0f;
        }
        storageService.changeStoragePercent(id, percent);
    }

    @ResponseBody
    @RequestMapping(value = "/deleteStorage", method = RequestMethod.POST)
    public String deleteStorage(@RequestParam("rowId")Long id){
        return storageService.deleteStorage(id);
    }
}
