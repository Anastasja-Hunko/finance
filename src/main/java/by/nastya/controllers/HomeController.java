package by.nastya.controllers;

import by.nastya.models.Envelop;
import by.nastya.services.*;
import by.nastya.utils.IdentityStorage;
import by.nastya.utils.UrlAddress;
import by.nastya.utils.View;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.*;

@Controller
public class HomeController {
    @Autowired
    private WorkWithDateService workWithDateService;
    @Autowired
    private HomeService homeService;
    @Autowired
    private OperationService operationService;
    @Autowired
    private DepositService depositService;

    @RequestMapping(value = UrlAddress.URL_ROOT, method = RequestMethod.GET)
    public ModelAndView main() {
        View view = new View("main");

        //сегодня
        Date workDate = new Date();
        view.addObject("date", workWithDateService.formatParamDate(workDate));

        //заработано
        BigDecimal incomingSum = operationService.getOperationSumForMonth(IdentityStorage.INCOMING_TYPE, workDate);
        view.addObject("incoming", incomingSum);

        // потрачено
        BigDecimal consumptionSum = operationService.getOperationSumForMonth(IdentityStorage.CONSUMPTION_TYPE, workDate);
        view.addObject("consumption", consumptionSum);

        // депозит
        Pair<BigDecimal, BigDecimal> sumPair = depositService.getInfoByDeposit(workDate);
        view.addObject("inter", sumPair.getKey());
        view.addObject("deposit", sumPair.getValue());

        // 4 конверта
        List<Envelop> envelopList = homeService.getEnvelopList();

        view.addObject("envelopTable", envelopList);
        view.addObject("envelopSize", envelopList.isEmpty());
        return view;
    }

    @RequestMapping(value = "/createFourEnvelops", method = RequestMethod.GET)
    public ModelAndView createFourEnvelops() {
        View partView = new View("createEnvelops.jsp");
        homeService.createFourEnvelopers(new Date());
        partView.addObject("envelopSize", false);
        return partView;
    }

}
