package by.nastya.controllers;

import by.nastya.models.CashFlow;
import by.nastya.models.Dictionary;
import by.nastya.repositories.CashFlowRepository;
import by.nastya.repositories.DictionaryRepository;
import by.nastya.services.OperationService;
import by.nastya.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Controller
public class OperationController {
    @Autowired
    private DictionaryRepository dictionaryRepository;
    @Autowired
    private CashFlowRepository cashFlowRepository;
    @Autowired
    private OperationService operationService;

    @RequestMapping(value = "/operationEnter", method = RequestMethod.GET)
    public ModelAndView getCash(){
        View view = new View("operation");
        List<Dictionary> typesOfOperation = dictionaryRepository.findByParentId(IdentityStorage.OPERATION_TYPE_PARENT);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(new Date());
        view.addObject("types", typesOfOperation);
        view.addObject("date", date);
        return view;
    }

    @RequestMapping(value = "/operationEnter", method = RequestMethod.POST)
    public String saveCash(@RequestParam("operation") Long operation, @RequestParam("dateEnter") String dateEnter,
                           @RequestParam("sum") BigDecimal sum, @RequestParam("source") Long source, RedirectAttributes atributes){
        operationService.saveCash(operation, dateEnter, sum, source);

        if(operation.equals(IdentityStorage.INCOMING_TYPE)){
            operationService.fillStorages(sum);
        }

        atributes.addFlashAttribute("success", "Успешно добавлено");

        return "redirect:/operationEnter";
    }

    @RequestMapping(value = "/operationEnter/getSources", method = RequestMethod.GET)
    @ResponseBody
    public List<Dictionary> getSources(Long value){
        List<Dictionary> list = dictionaryRepository.findByParentId(value);
        return list;
    }

    @RequestMapping(value= "/incomings", method = RequestMethod.GET)
    public ModelAndView getIncomingsList(){
        View view = new View("operationInfo");
        List<CashFlow> operationList = cashFlowRepository.findAllByTypeId(IdentityStorage.INCOMING_TYPE);
        view.addObject("list",  operationList);
        view.addObject("type", "Доходы");
        return view;
    }

    @RequestMapping(value= "/consumption", method = RequestMethod.GET)
    public ModelAndView getConsumptionList(){
        View view = new View("operationInfo");
        List<CashFlow> operationList = cashFlowRepository.findAllByTypeId(IdentityStorage.CONSUMPTION_TYPE);
        view.addObject("list",  operationList);
        view.addObject("type", "Расходы");
        return view;
    }


}
