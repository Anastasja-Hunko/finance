package by.nastya.services;

import by.nastya.models.CashFlow;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface OperationService {
    void saveCash(Long operation, String dateString, BigDecimal sum, Long source);

    void saveCash(Long operation, Date date, BigDecimal sum, Long source);

    void fillStorages(BigDecimal sum);

    BigDecimal sumOfList(List<CashFlow> list);

    BigDecimal getOperationSumForMonth(Long operationId, Date date);
}
