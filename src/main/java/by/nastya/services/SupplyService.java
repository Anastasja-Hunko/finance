package by.nastya.services;

import by.nastya.models.Supply;
import by.nastya.models.SupplyList;

import java.math.BigDecimal;
import java.util.List;

public interface SupplyService {
    List<SupplyList> findAll();

    void saveList(String name);

    List<Supply> getSuppliesList(Long id);

    void saveSupply(Long id, BigDecimal sum, String name);

    void transferForConsumption(Long id);

    void deleteList(Long id);

    void changePrice(Long id, BigDecimal sum);
}
