package by.nastya.services;

import by.nastya.models.Storage;

import java.util.List;

public interface StorageService {
    void saveStorage(String name, String limit, Float percent);

    List<Storage> getStorages();

    void changeStoragePercent(Long id, Float percent);

    String deleteStorage(Long id);

    long storagesCount();

    void saveStorage(Storage storage);
}
