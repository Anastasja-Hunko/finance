package by.nastya.services;

import by.nastya.models.User;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    Boolean registration(String login, String password, String email);
    Boolean sendRestoreRequest(User user, HttpServletRequest request);
    Boolean changePassword(User user, String password);
}
