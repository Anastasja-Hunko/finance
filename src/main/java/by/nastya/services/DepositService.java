package by.nastya.services;

import by.nastya.models.Deposit;
import javafx.util.Pair;

import java.math.BigDecimal;
import java.util.Date;

public interface DepositService {

    void saveDeposit(String sum, Float percent);

    void work(Deposit deposit);

    Pair<BigDecimal, BigDecimal> getInfoByDeposit(Date date);
}
