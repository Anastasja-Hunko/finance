package by.nastya.services;

import java.math.BigDecimal;

public interface WorkWithBigDecimal {
    BigDecimal convertToBigDecimal(String value);
}