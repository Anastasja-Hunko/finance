package by.nastya.services.Impl;

import by.nastya.models.Storage;
import by.nastya.repositories.StorageRepository;
import by.nastya.services.StorageService;
import by.nastya.services.WorkWithBigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class StorageServiceImpl implements StorageService {
    @Autowired
    private StorageRepository storageRepository;
    @Autowired
    private WorkWithBigDecimal workWithBigDecimal;

    public void getPercentFromIncoming(BigDecimal sum, Storage storage){
        BigDecimal newSum = sum.multiply(new BigDecimal(storage.getPercent()/100));
        Storage storage1 = storageRepository.findOne(storage.getId());
        if(storage1.getMoneyAmount() == null){
            storage1.setMoneyAmount(BigDecimal.ZERO);
        }
        storage1.setMoneyAmount(storage.getMoneyAmount().add(newSum));
        storageRepository.save(storage1);

    }

    public Boolean checkForFullStorage(BigDecimal limit, BigDecimal amount){
        Boolean result = null;
        int compareResult;
        compareResult = amount.compareTo(limit);
        if(compareResult >= 0){
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public void saveStorage(String name, String limit, Float percent){
        Storage storage = new Storage();

        storage.setStorageName(name);
        storage.setMoneyAmount(BigDecimal.ZERO);
        storage.setPercent(percent);
        storage.setStorageLimit(workWithBigDecimal.convertToBigDecimal(limit));
        saveStorage(storage);
    }

    public List<Storage> getStorages(){
        return storageRepository.findAll();
    }

    public void changeStoragePercent(Long id, Float percent){
        Storage storage = storageRepository.findOne(id);
        storage.setPercent(percent);
        storageRepository.save(storage);
    }

    public String deleteStorage(Long id){
        Storage storage = storageRepository.findOne(id);
        if(storage.getMoneyAmount().compareTo(BigDecimal.ZERO) > 0) {
            return "0";
        } else {
            storageRepository.delete(id);
            return "1";
        }
    }

    public long storagesCount(){
        return storageRepository.count();
    }


    public void saveStorage(Storage storage){
        storageRepository.save(storage);
    }
}
