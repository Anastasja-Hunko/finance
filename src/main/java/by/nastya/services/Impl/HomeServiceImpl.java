package by.nastya.services.Impl;

import by.nastya.models.Envelop;
import by.nastya.repositories.EnvelopRepository;
import by.nastya.services.HomeService;
import by.nastya.services.WorkWithDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class HomeServiceImpl implements HomeService {
    @Autowired
    private WorkWithDateService workWithDateService;
    @Autowired
    private EnvelopRepository envelopRepository;

    public void createFourEnvelopers(Date date) {
        List<Envelop> envelops = new ArrayList();
        Date endOfTime = workWithDateService.addMonthesToDate(1, date);
        long days = workWithDateService.lengthBetweenTwoDates(date, endOfTime);
        int period = (int)(days / 4);
        for(int i = 0; i < 4; i++){
            Date envelopStart = date;
            Date envelopEnd = workWithDateService.addDaysToDate(period, date);
            if(i == 3) envelopEnd = endOfTime;
            Envelop envelop = new Envelop();
            envelop.setBeginSum(BigDecimal.ZERO);
            envelop.setEditSum(BigDecimal.ZERO);
            envelop.setDateBegin(envelopStart);
            envelop.setDateEnd(envelopEnd);
            date = workWithDateService.addDaysToDate(1, envelopEnd);
            envelops.add(envelop);
        }
        envelopRepository.save(envelops);
    }

    public List<Envelop> getEnvelopList() {
        return envelopRepository.findAll();
    }
}
