package by.nastya.services.Impl;

import by.nastya.models.Storage;
import by.nastya.models.Supply;
import by.nastya.models.SupplyList;
import by.nastya.repositories.SupplyListRepository;
import by.nastya.repositories.SupplyRepository;
import by.nastya.services.OperationService;
import by.nastya.services.SupplyService;
import by.nastya.utils.IdentityStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class SupplyServiceImpl implements SupplyService {
    @Autowired
    private SupplyListRepository supplyListRepository;
    @Autowired
    private SupplyRepository supplyRepository;
    @Autowired
    private OperationService operationService;

    public List<SupplyList> findAll(){
        return supplyListRepository.findAll();
    }

    public void saveList(String name){
        SupplyList supplyList = new SupplyList();
        supplyList.setName(name);
        supplyList.setDate(new Date());
        supplyListRepository.save(supplyList);
    }

    public List<Supply> getSuppliesList(Long id) {
        return supplyRepository.findAllByListId(id);
    }

    public void saveSupply(Long id, BigDecimal sum, String name){
        Supply supply = new Supply();
        supply.setName(name);
        supply.setCost(sum);
        supply.setList(supplyListRepository.getOne(id));
        supplyRepository.save(supply);
    }

    public void transferForConsumption(Long id){
        List<Supply> list = supplyRepository.findAllByListId(id);
        BigDecimal sum = BigDecimal.ZERO;
        for(Supply supply : list){
            if(supply.getCost()!=null) {
                sum = sum.add(supply.getCost());
            }
        }
        operationService.saveCash(IdentityStorage.CONSUMPTION_TYPE, new Date(), sum, IdentityStorage.SHOP_SUBTYPE);
        supplyRepository.delete(list);
        supplyListRepository.delete(id);
    }

    public void deleteList(Long id) {
        supplyListRepository.delete(id);
    }

    public void changePrice(Long id, BigDecimal sum){
        Supply supply = supplyRepository.findOne(id);
        supply.setCost(sum);
        supplyRepository.save(supply);
    }
}
