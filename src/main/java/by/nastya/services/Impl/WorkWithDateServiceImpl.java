package by.nastya.services.Impl;

import by.nastya.services.WorkWithDateService;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class WorkWithDateServiceImpl implements WorkWithDateService {
    public int getMaxDayInMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public List<Integer> getDaysInMonth(Date date){
        List<Integer> days = new ArrayList<Integer>();

        for(int i = 1; i <= getMaxDayInMonth(date); i++) {
            days.add(i);
        }

        return days;
    }

    public Date setFirstDayOfMonth(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public Date setLastDayOfMonth(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, getMaxDayInMonth(date));
        return calendar.getTime();
    }

    public Date parseParamDate(String date) {
        if(date == null)
            return null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date s = new Date();
        try {
            s = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return s;
    }

    public String formatParamDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String s =  formatter.format(date);
        return s;
    }

    public Date addMonthesToDate(int monthes, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, monthes);
        return calendar.getTime();
    }

    public Date addDaysToDate(int days, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return calendar.getTime();
    }

    public long lengthBetweenTwoDates(Date dateStart, Date dateFinish){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateStart);
        Calendar calendarFinish = Calendar.getInstance();
        calendarFinish.setTime(dateFinish);
        long length = ((calendarFinish.getTimeInMillis() - calendar.getTimeInMillis()) / 1000 / 60 / 60 / 24) + 1;
        return length;
    }
}
