package by.nastya.services.Impl;

import by.nastya.models.User;
import by.nastya.models.UserContext;
import by.nastya.repositories.RoleRepository;
import by.nastya.repositories.UserRepository;
import by.nastya.services.UserService;
import by.nastya.utils.EmaiSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class UserServiceImpl implements UserService, AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private EmaiSender emailSender;

    public Boolean registration(String login, String password, String email) {
        if(userRepository.countByLogin(login) > 0L) {
            return false;
        } else {
            User user = new User();
            user.setLogin(login);
            user.setPassword(encoder.encode(password));
            user.setEmail(email);
            user.setRole(roleRepository.findByName("ROLE_USER"));
            return userRepository.save(user) != null;
        }
    }

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        User user = userRepository.findByLogin(authentication.getName());

        if (user == null || !encoder.matches(authentication.getCredentials().toString(), user.getPassword())) {
            throw new BadCredentialsException("Неверный логин или пароль");
        }

        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().getName()));

        UserContext userContext = new UserContext(user.getLogin(), user.getPassword(), grantedAuthorities, user);

        return new UsernamePasswordAuthenticationToken(userContext, authentication.getCredentials().toString(), grantedAuthorities);
    }

    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    public Boolean sendRestoreRequest(User user, HttpServletRequest request) {
        user.setToken(UUID.randomUUID().toString());
        userRepository.save(user);
        String url = request.getRequestURL().toString();
        String domain = url.substring(0, url.length() - request.getRequestURI().length())
                + request.getContextPath() + "/";
        String text = "Здравствуйте, " + user.getLogin() + ".<br/>" +
                "Для восстановления доступа к аккаунту перейдите по " +
                "<a href=\"" + domain + "restore/confirm/" + user.getToken() + "\">ссылке</a>";
        emailSender.sendMail("financeApp", user.getEmail(), "Восстановление доступа", text);
        return true;
    }

    public Boolean changePassword(User user, String password) {
        //Если новый и старый пароли совпадают
        if (encoder.matches(password, user.getPassword())) {
            return false;
        }
        user.setPassword(encoder.encode(password));
        user.setToken(null);
        return userRepository.save(user) != null;
    }
}
