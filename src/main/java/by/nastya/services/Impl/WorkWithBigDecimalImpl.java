package by.nastya.services.Impl;

import by.nastya.services.WorkWithBigDecimal;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

@Service
public class WorkWithBigDecimalImpl implements WorkWithBigDecimal {
    public BigDecimal convertToBigDecimal(String value) {
        if (value.equals("") || value.equals(null) || value.equals("NaN")) return BigDecimal.ZERO;
        else {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setGroupingSeparator(',');
            symbols.setDecimalSeparator('.');
            String pattern = "#,##0.0#";
            DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
            decimalFormat.setParseBigDecimal(true);
            BigDecimal summ = null;
            try {
                summ = ((BigDecimal) decimalFormat.parse(value));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return summ;
        }
    }
}
