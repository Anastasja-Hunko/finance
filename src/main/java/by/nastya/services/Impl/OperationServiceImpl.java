package by.nastya.services.Impl;

import by.nastya.models.CashFlow;
import by.nastya.models.Storage;
import by.nastya.repositories.CashFlowRepository;
import by.nastya.repositories.DictionaryRepository;
import by.nastya.services.OperationService;
import by.nastya.services.StorageService;
import by.nastya.services.WorkWithDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class OperationServiceImpl implements OperationService{
    @Autowired
    private CashFlowRepository cashFlowRepository;
    @Autowired
    private DictionaryRepository dictionaryRepository;
    @Autowired
    private WorkWithDateService workWithDateService;
    @Autowired
    private StorageService storageService;

    public void saveCash(Long operation, String dateString, BigDecimal sum, Long source){
        Date date = workWithDateService.parseParamDate(dateString);
        saveCash(operation, date, sum, source);
    }

    public void saveCash(Long operation, Date date, BigDecimal sum, Long source){
        CashFlow cashFlow = new CashFlow();
        cashFlow.setType(dictionaryRepository.getOne(operation));
        cashFlow.setDate(date);
        cashFlow.setMoneyAmount(sum);
        cashFlow.setSubtype(dictionaryRepository.getOne(source));
        cashFlowRepository.save(cashFlow);
    }

    public void fillStorages(BigDecimal sum){
        if(storageService.storagesCount() != 0) {
            for(Storage storage : storageService.getStorages()) {
                if(storage.getMoneyAmount().compareTo(storage.getStorageLimit()) == 0)
                    break;
                else {
                    BigDecimal restMoney = storage.getStorageLimit().subtract(storage.getMoneyAmount());
                    BigDecimal deltaStorage = sum.multiply(new BigDecimal(storage.getPercent()));

                    if(deltaStorage.compareTo(restMoney) >= 0) {
                        storage.setMoneyAmount(storage.getMoneyAmount().add(restMoney));
                        sum = sum.subtract(restMoney);
                    } else {
                        storage.setMoneyAmount(storage.getMoneyAmount().add(deltaStorage));
                        sum = sum.subtract(deltaStorage);
                    }
                    storageService.saveStorage(storage);
                }
            }
        }
    }

    public BigDecimal sumOfList(List<CashFlow> list){
        BigDecimal sum = BigDecimal.ZERO;
        for(CashFlow cashFlow : list){
            sum = sum.add(cashFlow.getMoneyAmount());
        }
        return sum;
    }

    public BigDecimal getOperationSumForMonth(Long operationId, Date date){
        List<CashFlow> operationList = cashFlowRepository.findAllByCurrentDateAndTypeId(workWithDateService.setFirstDayOfMonth(date), workWithDateService.setLastDayOfMonth(date), operationId);
        return sumOfList(operationList);
    }
}
