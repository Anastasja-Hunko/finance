package by.nastya.services.Impl;

import by.nastya.models.Deposit;
import by.nastya.models.DepositInfo;
import by.nastya.repositories.DepositInfoRepository;
import by.nastya.repositories.DepositRepository;
import by.nastya.repositories.DictionaryRepository;
import by.nastya.services.DepositService;
import by.nastya.services.WorkWithBigDecimal;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class DepositServiceImpl implements DepositService {
    @Autowired
    private DepositRepository depositRepository;
    @Autowired
    private DepositInfoRepository depositInfoRepository;
    @Autowired
    private WorkWithBigDecimal workWithBigDecimal;

    public Deposit createDeposit(BigDecimal sum, Date date) {
        Deposit deposit = new Deposit();
        deposit.setBeginSum(sum);
        deposit.setMoneyAmount(sum);
        deposit.setPercent(11f);
        deposit.setPercentSum(BigDecimal.ZERO);
        deposit.setDatebegin(date);
        return deposit;
    }

    public void saveDeposit(String sum, Float percent){
        Deposit deposit = new Deposit();
        BigDecimal bdSum = workWithBigDecimal.convertToBigDecimal(sum);
        deposit.setBeginSum(bdSum);
        deposit.setMoneyAmount(bdSum);
        deposit.setPercent(percent);
        deposit.setPercentSum(BigDecimal.ZERO);
        deposit.setDatebegin(new Date());
        depositRepository.save(deposit);
    }

    public void work(Deposit deposit){
        //Капитализация каждый день
        Date date = deposit.getDatebegin();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        BigDecimal beginSum = deposit.getBeginSum();
        int days = calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
        Float newPercent = deposit.getPercent()/days;
        for(int i = 1; i <= days; i++){
            DepositInfo depositInfo = new DepositInfo();
            depositInfo.setDeposit(deposit);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            date = calendar.getTime();
            depositInfo.setBeginSum(beginSum);
            depositInfo.setDate(date);
            deposit.setPercentSum(deposit.getMoneyAmount().multiply(new BigDecimal(newPercent/100)));
            depositInfo.setPercentSum(deposit.getPercentSum());
            deposit.setMoneyAmount(deposit.getMoneyAmount().add(deposit.getPercentSum()));
            depositInfo.setCalcSum(deposit.getMoneyAmount());
            depositInfoRepository.save(depositInfo);
        }
    }

    public BigDecimal sumThroughMonth(Date date, BigDecimal sum, Deposit deposit){
        BigDecimal newSum = BigDecimal.ZERO;
        if(!deposit.equals(null)) {
            Date dateStart = deposit.getDatebegin();
            Calendar finish = Calendar.getInstance();
            finish.add(Calendar.MONTH, 12);
            Date dateEnd = finish.getTime();
            if (date.compareTo(dateStart) >= 0 && date.compareTo(dateEnd) <= 0) {
                newSum = sum.multiply(new BigDecimal(deposit.getPercent() / 1200));
            }
        }
        return newSum;
    }

    public Pair<BigDecimal, BigDecimal> getInfoByDeposit(Date date) {
        List<Deposit> deposites = depositRepository.findAll();
        if(!deposites.isEmpty()) {
            Deposit deposit = deposites.get(0);
            BigDecimal currentSum = depositInfoRepository.getIntermediateSumByDateDeposit(date, deposit.getId());
            if(currentSum == null) currentSum = deposit.getBeginSum();
            BigDecimal beginSum = deposit.getBeginSum();
            BigDecimal deltaSum = currentSum.subtract(beginSum);
            return new Pair<BigDecimal, BigDecimal>(currentSum, deltaSum);
        } else {
            return new Pair<BigDecimal, BigDecimal>(BigDecimal.ZERO, BigDecimal.ZERO);
        }
    }

}

