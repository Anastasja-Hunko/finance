package by.nastya.services;

import by.nastya.models.Envelop;

import java.util.Date;
import java.util.List;

public interface HomeService {
    void createFourEnvelopers(Date date);

    List<Envelop> getEnvelopList();
}
