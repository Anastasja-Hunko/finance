package by.nastya.services;

import java.util.Date;
import java.util.List;

public interface WorkWithDateService {
    int getMaxDayInMonth(Date date);

    List<Integer> getDaysInMonth(Date date);

    Date setFirstDayOfMonth(Date date);

    Date setLastDayOfMonth(Date date);

    Date parseParamDate(String date);

    String formatParamDate(Date date);

    Date addMonthesToDate(int monthes, Date date);

    Date addDaysToDate(int days, Date date);

    long lengthBetweenTwoDates(Date dateStart, Date dateFinish);
}
