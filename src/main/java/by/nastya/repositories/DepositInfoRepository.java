package by.nastya.repositories;

import by.nastya.models.DepositInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;

@Repository
public interface DepositInfoRepository extends JpaRepository<DepositInfo, Long>{
    @Query("select t.calcSum from DepositInfo  t where t.date = ?1 and t.deposit.id = ?2")
    BigDecimal getIntermediateSumByDateDeposit(Date date, Long deposit);
}
