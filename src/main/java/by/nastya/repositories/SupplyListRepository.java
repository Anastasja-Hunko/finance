package by.nastya.repositories;

import by.nastya.models.SupplyList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplyListRepository extends JpaRepository<SupplyList, Long>{
}
