package by.nastya.repositories;

import by.nastya.models.Supply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplyRepository extends JpaRepository<Supply, Long> {
    @Query("select t from Supply t where t.list.id=?1")
    List<Supply> findAllByListId(Long listId);
}
