package by.nastya.repositories;

import by.nastya.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(String login);
    Long countByLogin(String login);
    User findByEmail(String email);
    User findByToken(String token);
}
