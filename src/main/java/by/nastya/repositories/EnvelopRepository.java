package by.nastya.repositories;

import by.nastya.models.Envelop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnvelopRepository extends JpaRepository<Envelop, Long> {
}
