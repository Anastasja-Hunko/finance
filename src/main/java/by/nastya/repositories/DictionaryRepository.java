package by.nastya.repositories;

import by.nastya.models.Dictionary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictionaryRepository extends JpaRepository<Dictionary, Long> {
    @Query("select t from Dictionary t where t.parent.id = ?1")
    List<Dictionary> findByParentId(Long parent);

}
