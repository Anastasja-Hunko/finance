package by.nastya.repositories;

import by.nastya.models.CashFlow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CashFlowRepository extends JpaRepository<CashFlow, Long> {
    List<CashFlow> findAllByTypeId(Long id);

    @Query("select t from CashFlow t where t.date <= ?2 and t.date >= ?1 and t.type.id =?3")
    List<CashFlow> findAllByCurrentDateAndTypeId(Date start, Date end, Long typeId);
}
