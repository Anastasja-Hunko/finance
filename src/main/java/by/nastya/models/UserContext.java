package by.nastya.models;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class UserContext extends org.springframework.security.core.userdetails.User {

    private User user;

    public UserContext(String login, String password, Collection<? extends GrantedAuthority> authorities,
                       User user) {
        super(login, password, authorities);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}