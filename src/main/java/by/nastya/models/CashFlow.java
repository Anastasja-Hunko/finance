package by.nastya.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table( name = "cashFlow")
public class CashFlow extends BaseEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    private Dictionary type;

    @Column(name = "dateEnter")
    private Date date;

    @Column(name = "moneyAmount")
    private BigDecimal moneyAmount;

    @ManyToOne(fetch = FetchType.EAGER)
    private Dictionary subtype;

    public Dictionary getType() {
        return type;
    }

    public void setType(Dictionary type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(BigDecimal moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public Dictionary getSubtype() {
        return subtype;
    }

    public void setSubtype(Dictionary subtype) {
        this.subtype = subtype;
    }

    public CashFlow() {
    }


}
