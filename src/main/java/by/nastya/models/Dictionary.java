package by.nastya.models;

import javax.persistence.*;

@Entity
@Table( name = "dictionary")
public class Dictionary extends BaseEntity {
    @Column( name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private Dictionary parent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dictionary getParent() {
        return parent;
    }

    public void setParent(Dictionary parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dictionary that = (Dictionary) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return parent != null ? parent.equals(that.parent) : that.parent == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        return result;
    }
}
