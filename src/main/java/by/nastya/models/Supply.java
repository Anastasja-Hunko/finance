package by.nastya.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "supply")
public class Supply extends BaseEntity {
    @Column(name = "name")
    private String name;

    @Column(name = "cost")
    private BigDecimal cost;

    @ManyToOne(fetch = FetchType.EAGER)
    private SupplyList list;

    public Supply(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public SupplyList getList() {
        return list;
    }

    public void setList(SupplyList list) {
        this.list = list;
    }
}
