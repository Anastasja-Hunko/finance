package by.nastya.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by nastya on 04.05.2017.
 */
@Entity
@Table(name="depositInfo")
public class DepositInfo extends BaseEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    private Deposit deposit;

    @Column(name = "calculation_date")
    private Date date;

    @Column(name = "begin")
    private BigDecimal beginSum;

    @Column(name = "intermediateSum")
    private BigDecimal calcSum;

    @Column (name="percentSum")
    private BigDecimal percentSum;

    public Deposit getDeposit() {
        return deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getBeginSum() {
        return beginSum;
    }

    public void setBeginSum(BigDecimal beginSum) {
        this.beginSum = beginSum;
    }

    public BigDecimal getCalcSum() {
        return calcSum;
    }

    public void setCalcSum(BigDecimal calcSum) {
        this.calcSum = calcSum;
    }

    public BigDecimal getPercentSum() {
        return percentSum;
    }

    public void setPercentSum(BigDecimal percentSum) {
        this.percentSum = percentSum;
    }
}
