package by.nastya.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table( name = "storage")
public class Storage extends BaseEntity {
    @Column(name = "name")
    private String storageName;

    @Column(name = "storageLimit")
    private BigDecimal storageLimit;

    @Column(name = "moneyAmount")
    private BigDecimal moneyAmount;

    @Column(name = "percent")
    private Float percent;

    public Storage(){}

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public BigDecimal getStorageLimit() {
        return storageLimit;
    }

    public void setStorageLimit(BigDecimal storageLimit) {
        this.storageLimit = storageLimit;
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(BigDecimal moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public Float getPercent() {
        return percent;
    }

    public void setPercent(Float percent) {
        this.percent = percent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Storage storage = (Storage) o;

        if (storageName != null ? !storageName.equals(storage.storageName) : storage.storageName != null) return false;
        if (storageLimit != null ? !storageLimit.equals(storage.storageLimit) : storage.storageLimit != null)
            return false;
        if (moneyAmount != null ? !moneyAmount.equals(storage.moneyAmount) : storage.moneyAmount != null) return false;
        return percent != null ? percent.equals(storage.percent) : storage.percent == null;

    }

    @Override
    public int hashCode() {
        int result = storageName != null ? storageName.hashCode() : 0;
        result = 31 * result + (storageLimit != null ? storageLimit.hashCode() : 0);
        result = 31 * result + (moneyAmount != null ? moneyAmount.hashCode() : 0);
        result = 31 * result + (percent != null ? percent.hashCode() : 0);
        return result;
    }
}
