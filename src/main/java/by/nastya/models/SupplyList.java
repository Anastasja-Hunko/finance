package by.nastya.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "supplyList")
public class SupplyList extends BaseEntity {
    @Column(name="name")
    private String name;
    @Column(name="date")
    private Date date;

    public SupplyList(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
