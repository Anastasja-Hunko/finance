package by.nastya.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="deposit")
public class Deposit extends BaseEntity{
    @Column(name = "beginSum")
    private BigDecimal beginSum;

    @Column(name = "moneyAmount")
    private BigDecimal moneyAmount;

    @Column(name = "percent")
    private Float percent;

    @Column(name = "dateEnter")
    private Date datebegin;

    @Column (name = "perCentSum")
    private BigDecimal percentSum;

    public Deposit(){}

    public BigDecimal getBeginSum() {
        return beginSum;
    }

    public void setBeginSum(BigDecimal beginSum) {
        this.beginSum = beginSum;
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(BigDecimal moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public Float getPercent() {
        return percent;
    }

    public void setPercent(Float percent) {
        this.percent = percent;
    }

    public Date getDatebegin() {
        return datebegin;
    }

    public void setDatebegin(Date datebegin) {
        this.datebegin = datebegin;
    }

    public BigDecimal getPercentSum() {
        return percentSum;
    }

    public void setPercentSum(BigDecimal percentSum) {
        this.percentSum = percentSum;
    }
}
