package by.nastya.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="envelop")
public class Envelop extends BaseEntity {
    @Column(name = "begin_sum")
    private BigDecimal beginSum;

    @Column(name = "edit_sum")
    private BigDecimal editSum;

    @Column
    private Date dateBegin;

    @Column
    private Date dateEnd;


    public BigDecimal getBeginSum() {
        return beginSum;
    }

    public void setBeginSum(BigDecimal beginSum) {
        this.beginSum = beginSum;
    }

    public BigDecimal getEditSum() {
        return editSum;
    }

    public void setEditSum(BigDecimal editSum) {
        this.editSum = editSum;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }
}
