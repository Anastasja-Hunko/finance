<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="login_wrapper">
    <div class="animate form login_form">
        <c:if test="${!empty success}">
            <div class="alert alert-success alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span>
                </button>
                    ${success}
            </div>
        </c:if>
        <section class="login_content">
            <form class="form-horizontal m-t-20" method="post" action="/restore">
                <c:if test="${empty info && empty error}">
                <div class="alert alert-info">
                    Введите <b>Email</b>, привязанный к аккаунту в системе
                </div>
                </c:if>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="email" name="email" class="form-control" placeholder="Email адрес"
                               required="required">
                    </div>
                </div>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-inverse btn-block text-uppercase waves-effect waves-light">
                            Восстановить
                        </button>
                    </div>
                </div>

                    <div class="clearfix"></div>
                    <br>

                    <div>
                        <h1><i class="fa fa-eur"></i> Миллионер</h1>
                        <p><jsp:include page="../template/footer.jsp"/></p>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>