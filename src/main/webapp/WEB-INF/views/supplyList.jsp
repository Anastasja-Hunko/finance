<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="calendar" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type="text/javascript" src="../../resources/project.js"></script>
<link rel="stylesheet" type="text/css" href="../../resources/jquery.dataTables.min.css">

<script type="text/javascript">
    $(document).ready(function () {
        $('#lists').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            language: {
                url: "../../resources/russian.json"
            }
        });
    });
</script>

<form action="/addlist" method="get">
    <h1 align="center">Что купить?</h1>
    <hr>
    <button type="submit">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        Добавить новый список
    </button>

    <table id="lists" class="display">
        <thead>
        <tr>
            <td>Название списка</td>
            <td>Дата создания списка</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${lists}" var="input">
            <tr id="row${input.id}">
                <td>${input.name}</td>
                <td><calendar:formatDate value="${input.date}"
                                         pattern="yyyy-MM-dd"/></td>
                <td>
                    <button type="button" class="glyphicon glyphicon-list-alt" aria-hidden="true"
                            onclick="location.href='/listContent/${input.id}'"></button>
                    <button type="button" class="fa fa-trash-o" aria-hidden="true"
                            onclick="supplyList.removeRow('lists', '/deletelist', ${input.id})"></button>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</form>


<script type="text/javascript" src="../../resources/vendors/datatables.net/js/jquery.dataTables.js"></script>