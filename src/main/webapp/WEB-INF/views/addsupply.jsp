<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="../../resources/project.js"></script>

<div class = "col-md-12 col-sm-12 col-xs-12">
    <h2>Добавить продукт</h2>
    <hr>
    <form id="demo-form2" action="/addsupply" method="post">
        <div>
            <h5> Название </h5>
            <input type="text" id="name" class="form-control" name="name">
        </div>
        <br>
        <div>
            <h5> Цена </h5>
            <input id="cost" name = "cost" type="number" step = "0.01" class="form-control"/>
        </div>
        <br>

        <div hidden >
            <label for="id"> идентификатор списка </label>
            <input id="id" type="number" name="id" class="form-control" value="${supplyid}" readonly >
        </div>
        <br>
        <button class="btn btn-primary" type="button" onclick="general.goBack()">Назад</button>
        <button class="btn btn-success" type="submit">Сохранить</button>
    </form>

</div>
