<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="../../resources/project.js"></script>

<div class = "col-md-12 col-sm-12 col-xs-12">
    <h2>Создать новое хранилище</h2>
    <hr>
    <form id="demo-form2" action="/storage/add" method="post">
        <div>
            <h5>Название хранилища</h5>
            <input class="form-control" type="text" id="storageName" name="storageName" required>
        </div>
        <div>
            <h5>Лимит</h5>
            <input class="form-control" type="text" id="storageLimit" name="storageLimit" required>
        </div>
        <div>
            <h5>Процент</h5>
            <input class="form-control" type="text" id="percent" name="percent" required>
        </div>

        <button class="btn btn-primary" type="button" onclick="general.goBack()">Назад</button>
        <button class="btn btn-success" type="submit">Сохранить</button>
    </form>

</div>
