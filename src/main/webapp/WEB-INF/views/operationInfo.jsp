<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="calendar" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" type="text/css" href="../../resources/jquery.dataTables.min.css">

<script type="text/javascript">
    $(document).ready(function(){
        $('#incomings').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            language: {
                url:"../../resources/russian.json"
            }
        });
    });
</script>
<h1>${type}</h1>
<hr>
<table id="incomings" class="display">
    <thead>
    <tr>
        <td>Дата</td>
        <td>Размер</td>
        <td>Источник</td>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${list}" var="input">
        <tr>
            <td><calendar:formatDate  value="${input.date}"
                                      pattern="yyyy-MM-dd"/></td>
            <td>${input.moneyAmount}</td>
            <td>${input.subtype.name}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<script type="text/javascript" src="../../resources/vendors/datatables.net/js/jquery.dataTables.js"></script>