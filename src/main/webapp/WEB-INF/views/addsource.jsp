<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form action="/addsource" method="post">
    <h1 align="center">Добавить новый источник</h1>
    <hr>

    <div align="center">
        <h5>Выберите вид  операции</h5>
        <select id="operation" name="operation" class="form-control">
            <option></option>

            <c:forEach items="${qwelist}" var="type" >
                <option value = "${type.id}">${type.name}</option>
            </c:forEach>
        </select>


        <h5>Источник</h5>
        <input type="text" id="source" name = "source" class="form-control">

        <br>
        <br>

        <button class="btn btn-default submit" type="submit" style="width:100%;background: #2A3F54;color:#fff;">
            <span class="glyphicon glyphicon glyphicon-ok" aria-hidden="true"></span>
            Cохранить
        </button>
    </div>
</form>