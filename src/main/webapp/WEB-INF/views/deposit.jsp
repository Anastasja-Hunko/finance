<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="calendar" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" type="text/css" href="../../resources/jquery.dataTables.min.css">


<script type="text/javascript" src="../../resources/Chart.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#deposit').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            language: {
                url:"../../resources/russian.json"
            }
        });
    });

    $('#deposit tbody')
        .on( 'mouseenter', 'td', function () {
            var colIdx = table.cell(this).index().column;

            $( table.cells().nodes() ).removeClass( 'highlight' );
            $( table.column( colIdx ).nodes() ).addClass( 'highlight' );
        } );
</script>



    <h1 align="center">Информация о депозите</h1>


<div class="row">
    <c:if test="${!empty message}">${message}</c:if>

    <div class="col-md-7">

        <h5>Сумма вклада</h5>
        <input type="number" id="beginSum" name="beginSum" style="width: 100%" width = "300" class="form-control " value="${beginSum}"  readonly>

        <h5>Дата начала</h5>
        <input type = "text" id="dateBegin" name="dateBegin" style="width: 100%" width = "300" value = "${date}" class="form-control" readonly>

        <h5>Дата конца</h5>
        <input type = "text" id="dateEnd" name="dateEnd" style="width: 100%" width = "300" value = "${dateEnd}" class="form-control" readonly>

        <h5>Процентная ставка</h5>
        <input type="number" id="percent" name="percent" style="width: 100%" width = "300" value = "${percent}" class="form-control" readonly>

    </div>
    <div class="col-md-5">
        <canvas id="depositGraph" width="150" height="100"></canvas>
    </div>

    <script>
        var data = {
            labels: [
                "Сумма на конец депозита",
                "Начальная сумма"
            ],
            datasets: [
                {
                    data: [${total}, ${beginSum}],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB"
                    ]
                }]
        };

        var ctx = document.getElementById("depositGraph");

        var myDoughnutChart = new Chart(ctx, {
            type: 'doughnut',
            data: data,
            options: {
                animation:{
                    animateScale:true
                }
            }
        });
    </script>

    <br>
    <br>
        <h3 align="center">История депозита</h3>

        <table id="deposit">
            <thead>
            <tr>
                <td>Дата</td>
                <td>Начальная сумма</td>
                <td>Новая сумма</td>
                <td>Сумма начисленная</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${info}" var="item">
                <tr>
                    <td><calendar:formatDate  value="${item.date}"
                                              pattern="yyyy-MM-dd"/></td>
                    <td>${item.beginSum}</td>
                    <td>${item.calcSum}</td>
                    <td>${item.percentSum}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    <script type="text/javascript" src="../../resources/vendors/datatables.net/js/jquery.dataTables.js"></script>

</div>