<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css" href="../../resources/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../../resources/buttons.dataTables.min.css">
<script type="text/javascript" src="../../resources/project.js"></script>
<script type="text/javascript" src="../../resources/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../../resources/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../resources/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="../../resources/jszip.min.js"></script>
<script type="text/javascript" src="../../resources/pdfmake.min.js"></script>
<script type="text/javascript" src="../../resources/vfs_fonts.js"></script>
<script type="text/javascript" src="../../resources/buttons.html5.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.editable').click(function (e) {
            e.stopPropagation();
            var value = $(this).html();
            updateVal(this, value);
        });

        function updateVal(currentEle, value) {
            $(currentEle).html('<input class="thVal" maxlength="4" type="text" width="2" value="' + value + '" />');
            $(".thVal", currentEle).focus().keyup(function (event) {
                if (event.keyCode == 13) {
                    $.post('/addPrice', {
                        id: $(currentEle).parent().attr('id').split('row')[1],
                        value: $(".thVal").val().trim()
                    }, function (d) {
                        $(currentEle).html($(".thVal").val().trim());
                    });
                }
            }).click(function(e) {
                e.stopPropagation();
            });

            $(document).click(function() {
                $(".thVal").replaceWith(function() {
                    $.post('/addPrice', {
                        id: $(this).parent().parent().attr('id').split('row')[1],
                        value: this.value
                    }, function (d) {
                        return this.value;
                    });
                });
            });
        };



        $('#supplies').DataTable({
            dom: 'Bfrtip',
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {
                    extend: 'pdfHtml5',
                    download: 'download',
                    text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF',
                    orientation: 'landscape',
                }
            ],
            language: {
                url:"../../resources/russian.json"
            }
        });
    });

        function toConsumption(listid){
            $('#paramId').val(listid);
            $('#qqqForm').submit();
        }

</script>

<form method="post" action="/qqq" id="qqqForm">
    <input name="param" id="paramId" type="hidden" value="" />
</form>

<form action="/addsupply" method="get">
    <h3 align="center">Список покупок</h3>



    <table id="supplies" class="display">
        <thead>
        <tr>
            <td>Название продукта</td>
            <td>Цена</td>
            <td>
                <a href="/addsupply?per=${supplyid}" class="glyphicon glyphicon-plus" aria-hidden="true"></a>
            </td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${list}" var="input">
            <tr id="row${input.id}">
                <td>${input.name}</td>
                <td class="editable">${input.cost}</td>
                <td></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <button class="btn btn-primary" type="button" onclick="general.goBack()">Назад</button>
    <button class="btn btn-success" type="button" onclick="toConsumption(${supplyid})">Внести в расходы</button>

</form>


