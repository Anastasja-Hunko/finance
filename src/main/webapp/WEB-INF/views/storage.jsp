<%@ page contentType="text/html;charset=UTF-8" %>

<script type="text/javascript" src="../../resources/project.js"></script>
<link rel="stylesheet" type="text/css" href="../../resources/jquery.dataTables.min.css">
<script type="text/javascript" src="../../resources/vendors/datatables.net/js/jquery.dataTables.js"></script>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    $(document).ready(function(){
        $('#storageInfo').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            language: {
                url:"../../resources/russian.json"
            }
        });
    });
</script>

<div onkeydown="storage.enter(event)">
    <h1 align="center">Информация о хранилище</h1>
    <hr>
    <div class="row">
        <table id="storageInfo">
            <thead>
            <tr>
                <td><a href="/storage/add" class="glyphicon glyphicon-plus" aria-hidden="true"></a></td>
                <td>Наименование</td>
                <td>Процент</td>
                <td>Лимит</td>
                <td>Сколько лежит?</td>
                <td>Сколько осталось положить?</td>

            </tr>
            </thead>
            <tbody>
            <c:if test="${fn:length(storages) gt 0}">
                <c:forEach items="${storages}" var="storage">
                    <tr id="${storage.id}">
                        <td class="dropdown" align="center">
                            <div class="dropdown inline-block">
                                <i class="fa fa-cog fa-lg padding-gear pointer dropdown-toggle"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></i>
                                <ul class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownSend">
                                    <li>
                                        <a onclick="storage.changePercent('${storage.id}')"><span
                                                class="glyphicon glyphicon-pencil"></span> Редактировать процент</a></li>
                                    <li><a onclick="if (confirm('Вы уверены, что хотите безвозвратно удалить запись?'))
                                            {storage.deleteStorage('${storage.id}')}"><span
                                            class="glyphicon glyphicon-trash"></span> Удалить</a></li>
                                </ul>
                            </div>
                        </td>
                        <td>${storage.storageName}</td>
                        <td id="percent">${storage.percent}</td>
                        <td>${storage.storageLimit}</td>
                        <td>${storage.moneyAmount}</td>
                        <td>${storage.storageLimit - storage.moneyAmount}</td>
                    </tr>
                </c:forEach>
            </c:if>
            </tbody>
        </table>

            <div class="col-md-5">
                <canvas id="storageChart" width="150" height="100" style="display: none"></canvas>
            </div>

        <script>

        </script>
    </div>
</div>

