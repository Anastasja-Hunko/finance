<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<security:authorize access="isAnonymous()" var="isAnonymous"/>
<!-- footer content -->
<c:if test="${!isAnonymous}">
<footer>
    <div class="pull-right">
</c:if>
©2017-2019 - Система управления персональной финансовой информацией "Миллионер". Гунько Анастасия
<c:if test="${!isAnonymous}">
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</c:if>