<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<security:authorize access="isAnonymous()" var="isAnonymous"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Миллионер | </title>

    <!-- Bootstrap -->
    <link href="/resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/resources/custom.css" rel="stylesheet">
    <link href="/resources/style.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="/resources/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/resources/custom.js"></script>
</head>

<body class="${!isAnonymous ? 'nav-md' : 'login'}">
<div class="container body">
    <div class="main_container">

        <c:if test="${!isAnonymous}">
            <jsp:include page="sidebar.jsp"/>
            <jsp:include page="header.jsp"/>
        </c:if>

        <!-- page content -->
        <div class="right_col" role="main">
            <jsp:include page="${content}"/>
        </div>

        <c:if test="${!isAnonymous}">
            <jsp:include page="footer.jsp"/>
        </c:if>
    </div>
</div>
</body>
</html>
