<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="/" class="site_title"><i class="fa fa-eur"></i> <span>Миллионер</span></a>
        </div>
        <br>
        <br>
        <br>
        <br>
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li><a href="/operationEnter"><i class="fa fa-refresh"></i> Ввод доходов/расходов</a></li>
                </ul>
                <ul class="nav side-menu">
                    <li><a href="/management"><i class="fa fa-cogs"></i> Управление</a></li>
                </ul>
                <ul class="nav side-menu">
                    <li><a href="/incomings"><i class="fa fa-plus-square-o"></i> Доходы</a></li>
                </ul>
                <ul class="nav side-menu">
                    <li><a href="/consumption"><i class="fa fa-minus-square-o"></i> Расходы</a></li>
                </ul>
                <ul class="nav side-menu">
                    <li><a href="/storages"><i class="fa fa-university"></i> Хранилище</a></li>
                </ul>
                <ul class="nav side-menu">
                    <li><a href="/supplyList"><i class="fa fa-check-square-o"></i> Список покупок</a></li>
                </ul>
                <ul class="nav side-menu">
                    <li><a href="/deposit"><i class="fa fa-gift"></i> Депозит</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>