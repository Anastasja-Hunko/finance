<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="../../resources/vendors/datatables.net/js/jquery.dataTables.js"></script>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" type="text/css" href="../../resources/jquery.dataTables.min.css">
<%@ taglib prefix="calendar" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script>
    $(document).ready(function () {
        $('#envelops').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            language: {
                url: "../../resources/russian.json"
            }
        });
    });
</script>

<div class=" flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
        <div class="count">${date}</div>

        <h3>Сегодня</h3>
        <p>:)</p>
    </div>
</div>

<div class="flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="tile-stats">
    <div class="count">${incoming}</div>
    <h3>Было заработано</h3>
    <p>в этом месяце.</p>
</div>
</div>

<div class="flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="tile-stats">
    <div class="count">${consumption}</div>

    <h3>Было потрачено</h3>
    <p>в этом месяце.</p>
</div>
</div>

<div class="flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="tile-stats">
    <div class="count">${inter}</div>

    <h3>На депозите.</h3>
    <p>Заработано ${deposit} рублей</p>
</div>
</div>

<jsp:include page="createEnvelops.jsp"/>

<table id="envelops" class="display">
    <thead>
    <tr>
        <td>Начальная сумма</td>
        <td>Текущая сумма</td>
        <td>Дата начала</td>
        <td>Дата конца</td>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${envelopTable}" var="input">
        <tr id="${input.id}">
            <td>${input.beginSum}</td>
            <td>${input.editSum}</td>
            <td><calendar:formatDate value="${input.dateBegin}"
                                     pattern="yyyy-MM-dd"/></td>
            <td><calendar:formatDate value="${input.dateEnd}"
                                     pattern="yyyy-MM-dd"/></td>
        </tr>
    </c:forEach>
    </tbody>
</table>