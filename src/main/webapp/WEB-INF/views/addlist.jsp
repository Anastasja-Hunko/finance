<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="../../resources/project.js"></script>

<div class = "col-md-12 col-sm-12 col-xs-12">
    <h2>Создать новый список покупок</h2>
    <hr>
        <form id="demo-form2" action="/addlist" method="post">
            <div>
                <h5> Название списка </h5>
                <input class="form-control" type="text" id="listname" name="listname">
            </div>
            <br>
            <div >
                <h5> Дата создания </h5>
                <input type="text" class="form-control" id="date" value="${current}" name="date" readonly>
            </div>
            <br>
            <button class="btn btn-primary" type="button" onclick="general.goBack()">Назад</button>
            <button class="btn btn-success" type="submit">Сохранить</button>
        </form>

</div>