<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css" href="../../resources/jquery.dataTables.min.css">
<%@ taglib prefix="calendar" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    $(document).ready(function(){
        $('#operationSource').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            language: {
                url:"../../resources/russian.json"
            }
        });

        $('#deposit').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            language: {
                url:"../../resources/russian.json"
            }
        });
    });

</script>
    <h1 align="center">Управление</h1>
    <hr>
    <h5>Источники доходов/ расходов</h5>
    <table id="operationSource">
        <thead>
        <tr>
            <td> <a href="/addsource" class="glyphicon glyphicon-plus" aria-hidden="true"></a></td>
            <td>Тип операции</td>
            <td>Тип источника</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${list}" var="point">
            <tr>
                <td></td>
                <td>${point.parent.name}</td>
                <td>${point.name}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <hr>

    <h5>Управление депозитом</h5>

    <table id="deposit">
        <thead>
        <tr>
            <td>
                <c:choose>
                    <c:when test="${fn:length(depositlist) gt 0}">
                        Пока нельзя добавить больше одного депозита
                    </c:when>
                    <c:otherwise>
                        <a href="/deposit/add" class="glyphicon glyphicon-plus" aria-hidden="true"></a>
                    </c:otherwise>
                </c:choose>
            </td>
            <td>Дата открытия</td>
            <td>Начальная сумма</td>
            <td>% депозита</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${depositlist}" var="point">
            <tr>
                <td></td>
                <td><calendar:formatDate  value="${point.datebegin}"
                                          pattern="yyyy-MM-dd"/></td>
                <td>${point.beginSum}</td>
                <td>${point.percent}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <hr>
<script type="text/javascript" src="../../resources/vendors/datatables.net/js/jquery.dataTables.js"></script>



