<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="../../resources/project.js"></script>

<style>
    #savebutton {
        width: 55% !important;
    }
</style>

<form action="/operationEnter" method="post">
    <h1 align="center">Ввод данных</h1>
    <hr>
    <c:if test="${!empty success}">${success}</c:if>

<div align="center">
    <h5>Выберите вид вводимой операции</h5>
    <select id="operation" name="operation" class="form-control" onchange="operation.changeSource(this.value)">
        <option></option>

        <c:forEach items="${types}" var="type" >
            <option value = "${type.id}">${type.name}</option>
        </c:forEach>
    </select>


    <h5>Дата операции</h5>
    <input id="dateEnter" name="dateEnter" type="date" class="form-control" value="${date}" readonly/>

    <h5>Сумма операции</h5>
    <input id="sum" name = "sum" type="number" step = "0.001" class="form-control"/>

    <h5>Источник</h5>
    <select id="source" name="source" class="form-control"></select>

    <br>
    <br>

    <button class="btn btn-default submit" type="submit" style="width:100%;background: #2A3F54;color:#fff;" id="savebutton">
        <span class="glyphicon glyphicon glyphicon-ok" aria-hidden="true"></span>
        Cохранить
    </button>
</div>
</form>


