<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="login_wrapper">
    <div class="animate form login_form">
        <c:if test="${!empty success}">
            <div class="alert alert-success alert-styled-left alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span>
                    </button>
                    ${success}
            </div>
        </c:if>
        <section class="login_content">
            <form method="post" action="/login">
                <h1>Вход в аккаунт</h1>
                <hr>
                <c:if test="${!empty error and !empty sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                            ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
                    </div>
                </c:if>
                <div>
                    <input type="text" name="username" class="form-control" placeholder="Логин" required="">
                </div>
                <div>
                    <input type="password" name="password" class="form-control" placeholder="Пароль" required="">
                </div>
                <div>
                    <button class="btn btn-default submit" type="submit" style="width:100%;background: #2A3F54;color:#fff;">Вход</button>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <p class="change_link">
                        <a href="/registration" class="to_register"> Регистрация </a> <span style="margin: 10px 15px 0 0;">|</span> <a href="/restore" class="to_register"> Восстановление пароля </a>
                    </p>

                    <div class="clearfix"></div>
                    <br>

                    <div>
                        <h1><i class="fa fa-eur"></i> Миллионер</h1>
                        <p><jsp:include page="template/footer.jsp"/></p>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
