var general = {
    goBack: function () {
        window.history.back();
    }
};

var planning = {
    createFourEnvelops: function () {
        $.ajax({
            url: '/createFourEnvelops',
            async: false
        })
    }
};

var operation = {
    changeSource: function (value) {
        $.getJSON("/operationEnter/getSources", {value: value}, function (data) {
            var select = "";
            $.each(data, function (i, item) {
                select += "<option value='" + item.id + "'>" + item.name + "</option>"
            });
            $('#source').html(select);
        });
    }
};

var supplyList = {
    removeRow: function (tableId, url, rowId) {
        $.post(url, {id: rowId}, function () {
            var t = $('#' + tableId).dataTable();
            var r = $("#row" + rowId);
            var aPos = t.fnGetPosition(r);
            t.fnDeleteRow(aPos);
        });
    }
}

var storage = {
    enter: function (e) {
        if (e.which == 13) {
            this.collectAndTransfer();
        }
    },

    collectAndTransfer: function () {
        var array = document.getElementsByClassName('thVal');
        var result = '';
        if (array.length != 0) {
            for (i = 0; i < array.length; i++) {
                result = $(".thVal").val();
                var a = $(".thVal").parent().parent().attr("id");

                $(array[i]).replaceWith(result);

                $.post('/changePercent', {
                    rowId: a,
                    value: result
                });
            }
        }
    },

    changePercent: function (val) {
        var row = $('#' + val + '');
        this.collectAndTransfer();
        var children = row.find("td[id='percent']");
        if (children.length != 0) {
            $.each(children, function (i, item) {
                var value = $(item).html();
                $(item).html('<input class="thVal" type="text" width="2"  value="' + value + '" />')
            });
        }

        $(".thVal", children).focus().keyup().click(function (e) {
            e.stopPropagation();
        });
    },

    deleteStorage: function (val) {
        $.post('/deleteStorage', {
            rowId: val
        }, function (d) {
            if (d === '0') alert("В хранилище есть деньги. Удаление невозможно");
            else location.reload();
        });
    }
};